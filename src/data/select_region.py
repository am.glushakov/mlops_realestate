import pandas as pd
import click


def select_region(
        input_path: str,
        output_path: str,
        region: int
) -> None:
    """Function selects the listings belong to a specified region.
    :param input_path: input_path
    :param output_path: output_path
    :param region: region  # 2661
    :return
    """
    df = pd.read_csv(input_path)
    df = df[df['region'] == region]
    df.drop('region', axis=1, inplace=True)
    print(f'Selected {len(df)} samples in region {region}')

    df.to_csv(output_path)


@click.command()
@click.argument("input_path", type=click.Path(exists=True))
@click.argument("output_path", type=click.Path())
@click.argument("region", type=click.INT)
def main(input_path, output_path, region):
    select_region(input_path, output_path, region)


if __name__ == "__main__":
    main()
