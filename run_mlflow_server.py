import subprocess
import click


def run_mlflow(backend_store, artifact_root, host):
    """
    Run mlflow process
    """
    subprocess.run(
        f"mlflow server --backend-store-uri {backend_store} --default-artifact-root"
        f" {artifact_root} --host {host}",
        shell=True,
    )


@click.command()
@click.argument("backend_store", default="sqlite:///mlflow.db")
@click.argument("artifact_root", default="./mlruns/artifacts")
@click.argument("host", default="127.0.0.1")
def main(backend_store, artifact_root, host):
    run_mlflow(backend_store, artifact_root, host)


if __name__ == "__main__":
    main()
